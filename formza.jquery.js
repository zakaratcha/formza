(function($){

$.fn.formza = function(config){
    if (document.all && !document.addEventListener) return this; // ie8-

    //config defaults
    config = $.extend({
        types: ['checkbox','radio','select','file'],
        classPrefix: 'formza',
        fileButtonText: 'Browse',
        inheritCSS: true
    }, config || {});

    $(this).find("input,button,select").andSelf().each(function(){
        var $this = $(this),
            tag = $this.get(0).tagName.toLowerCase(),
            type = $this.attr("type") ? $this.attr("type").toLowerCase() : '';
        if (tag === 'input' & !type) type = 'text';
        if (!$this.data('formza_formed') && (~config.types.indexOf(tag) || ~config.types.indexOf(type))){
            var obj = new $.formza_obj(this,tag,type,config);
            obj.change();
            obj.el.data('formza_formed',true);
        }
    });
    return this;
};

$.extend({formza_obj: function(el,tag,type,config){
    this.el = $(el);
    this.tag = tag;
    this.type = type;
    this.config = config;
}});

$.formza_obj.prototype.change = function(){
    var elCSS = this._getCSS();
    if ((this.tag === 'input') & (this.type === 'checkbox')) this._changeHTML('checkbox');
    if ((this.tag === 'input') & (this.type === 'radio')) this._changeHTML('radio');
    if ((this.tag === 'input') & (this.type === 'file')) this._changeHTML('file');
    if (this.tag === 'select') this._changeHTML('select');
    this._inheritCSS(elCSS);
};

$.formza_obj.prototype._getCSS = function(){
    if (!this.config.inheritCSS) return;
    return {
        marginTop:      this.el.css('marginTop'),
        marginRight:    this.el.css('marginRight'),
        marginBottom:   this.el.css('marginBottom'),
        marginLeft:     this.el.css('marginLeft'),
        position:       this.el.css('position') === 'absolute' ? 'absolute' : 'relative',
        top:            this.el.css('top'),
        right:          this.el.css('right'),
        bottom:         this.el.css('bottom'),
        left:           this.el.css('left')
    };
};

$.formza_obj.prototype._inheritCSS = function(elCSS){
    if (!this.config.inheritCSS) return;
    this.el.parent().css(elCSS);

    this.el.css({
        margin:         0,
        position:       this.tag === 'select' ? 'static' : 'absolute',
        top:            '-3px',
        right:          0,
        bottom:         0,
        left:           '-3px'
    });
};

$.formza_obj.prototype._changeHTML = function(type){
    var className = this.config.classPrefix + '-' + type;

    this.el
        .addClass(className + '__el')
        .wrap($(document.createElement('span')).addClass(className))
        .after($(document.createElement('span')).addClass(className + '__inner'));

    if (type === 'file'){
        this.el
            .after($(document.createElement('span'))
                .addClass(className + '__button')
                .html(this.config.fileButtonText))
            .on('change', function(){
                $(this)
                    .siblings('.' + className + '__inner')
                    .html($(this).val() ? $(this).val().split(/\/|\\/).pop() : '');
            });
    }

    if (type === 'select'){
        this.el.on('change', function(){
            $(this)
                .next('.' + className + '__inner')
                .html($(this).find('option:selected').html());
        }).change();
    }
};

})(jQuery);